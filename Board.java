import java.util.Random;

public class Board {
	
	//Fields
	private Tile[][] grid;
	private final int SIZE = 5;
	
	//Constructor
	public Board() {
		
		Random rng = new Random();
		grid = new Tile[SIZE][SIZE];
		
		for(int i = 0; i < SIZE; i++) {
			
			int rngIndex = rng.nextInt(grid[i].length);
			
			for(int j = 0; j < SIZE; j++) {
				if(j == rngIndex){
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else {
					grid[i][j] = Tile.BLANK;
				}
			}
		}
	}
	
	//Methods
	public String toString() {
		String result = "";
		for(int i = 0; i < SIZE; i++) {
			for(int j = 0; j < SIZE; j++) {
				result += grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	
	public int placeToken(int row, int col) {
		if(row > SIZE || row < 0 || col > SIZE || col < 0) {
			System.out.println("INVALID ROW OR COL!");
			return -2;
		}
		else if(grid[row][col].equals(Tile.CASTLE) || grid[row][col].equals(Tile.WALL)) {
			return -1;
		}
		else if(grid[row][col].equals(Tile.HIDDEN_WALL)) {
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}	

}
