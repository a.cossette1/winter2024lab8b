import java.util.Scanner;

public class BoardGameApp {
	public static void main(String[] args) {
		
		Board board = new Board();
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Hello Player!");
		System.out.println("The Rule for this game are as follows:");
		System.out.println("At the start of the game there is a 5 x 5 board. The Board is made up of blank tile and wall tiles.");
		System.out.println("Wall Tiles are randomly placed on the board. At the start of the game the walls are hidden.");
		System.out.println("You have 7 castle tiles that you must place around the board. You have 8 turns to place all your castle tiles on the board.");
		System.out.println("At each turn, you much choose where you want to play the castle");
		System.out.println("If there is a hidden wall in that position, a castle cannot be placed there. The wall is no longer hidden, and you lose a turn.");
		System.out.println("If there is already a castle at the position, a new castle tile cannot be placed on top. You can try again and you do not lose a turn.");
		System.out.println("If the position is blank, a castle tile will be placed");
		System.out.println("To win you must place all your castles at the end of the 8 turns. If not, you lose.");
		
		int numCastles = 7;
		int turns = 0;
		
		while(numCastles > 0 && turns < 8) {
			System.out.println("\n" + "You have done " + turns + " turns and have " + numCastles + " castles left to be placed.");
			System.out.println("Here is the current status of the board:");
			System.out.println("\n" + board);
			
			System.out.print("Please enter a integer between 0 and 4 to represent your row: ");
				int row = Integer.parseInt(reader.nextLine());
			System.out.print("Please enter a integer between 0 and 4 to represent your col: ");
				int col = Integer.parseInt(reader.nextLine());
				
			int position = board.placeToken(row, col);
			
			while(position < 0) {
				System.out.println("Please Re-Enter your inputs, make sure they are within the right numbers.");
				row = Integer.parseInt(reader.nextLine());
				col = Integer.parseInt(reader.nextLine());
				
				position = board.placeToken(row, col);
			}
			
			if(position == 1) {
				System.out.println("\n" + "There was a wall in that position :(");
			}
			else if(position == 0) {
				System.out.println("\n" + "Good job you found a blank spot and your tile was succesfully placed.");
				numCastles--;
			}
			
			turns++;
		}
		
		System.out.println(board);
		
		if(numCastles == 0) {
			System.out.println("Good job you have won");
		}
		else {
			System.out.println("Sorry you lost :( Better luck next time");
		}
	}
}