public enum Tile {
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	//Fields
	private final String name;
	
	//Constructor
	private Tile(String name) {
		this.name = name;
	}
	//Getter
	public String getName() {
		return this.name;
	}
}